<section id="apropos" class="disp-col-start centVH">

    <div class="disp-between-top padx2">
        <div>
            <h2 class="txt-succ py-3">Yannick Tindy</h2>
        </div>
        <div class="disp-col-center-end py-2">
            <p class="txt-dl m-0">06 29 74 18 69</p>
            <p class="txt-dl m-0">Mobilité Loire / Lyon</p>
            <p class="txt-dl m-0">yannicktindy@gmail.com</p>
        </div>
        
    </div>
    <hr>

    <div class="bando">
        <img class="img-fluid" src="img/16-9/band0.png" alt="back1">    
        <div class="bando-txt p-3">
            <h1 class="txt-lum">Développeur Web</h1>
            <h2 class="txt-lum">Fullstack / Back-End+</h2>
        </div>
    </div>

    <div class="disp-center mt-5 pt-3 padx2">
        <div class="butn butn-glow p-3 bk-dark text-center">
            <h3 class="txt-succ">Recherche Alternance </h3>
            <p class="txt-light m-0">Concepteur / Développeur d'Application (Bac+3/4)</p>
            <p class="txt-light">Septembre 2023</p>
        </div>
    </div> 

    
</section>

<?php include 'php/cvContent/cda.php'; ?>
<div id="symfony"></div>
<?php include 'php/cvContent/symfony.php'; ?>
<?php include 'php/cvContent/diplomes.php'; ?>
<?php include 'php/cvContent/experience.php'; ?>
<?php include 'php/cvContent/presentation.php'; ?>
<?php include 'php/cvContent/info.php'; ?>



