<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cv Yannick Tindy</title>
        <meta name="author" content="Yannick Tindy">
        <meta property="og:type" content="Yannick Tindy">
        <meta property="og:title" content="Yannick Tindy - Développeur Web / Web mobile Fullstack (Bac+2)">
        <meta property="og:url" content="https://silenus.fr">
        <meta property="og:image" content="https://silenus.fr/img/square/profile.jpg">
        <meta property="og:description" content="Objectif : Concepteur / Développeur d'Applications. Recherche d'un contrat en alternance (Bac+3/4)">
        <meta name="theme-color" content="#00FF00">
        <link rel="icon" type="image/x-icon" href="img/square/favicon.png" />

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">

        <link rel="stylesheet" href="../../style/mainBase/color.css">
        <link rel="stylesheet" href="../../style/mainBase/font.css">        
        <link rel="stylesheet" href="../../style/mainBase/margin.css">
        <link rel="stylesheet" href="../../style/mainBase/display.css">

        <link rel="stylesheet" href="../../style/components/buttons.css">

        <link rel="stylesheet" href="../../style/dark.css">
        <link rel="stylesheet" href="../../style/light.css">
        <link rel="stylesheet" href="../../style/aside.css">
        <link rel="stylesheet" href="../../style/main.css">
        <link rel="stylesheet" href="../../style/general.css">

        <link rel="stylesheet" href="../../style/werk/werk.css">
        <link rel="stylesheet" href="../../style/werk/nav.css">

    </head>

    <body class="dark">
        <div class="werk-container">
            <div class="werk-trunk">
                <header>
                    <div class="werk-nav fluid pad1">
                        <div class="row">
                            <div class="col-3">
                                <div class="nav-logo">
                                    
                                </div>
                            </div>
                            <div class="col-9">
                                <div class="nav-content">
                                    <div class="nav-up">
                                        <h4 class="mx-2 my-0 txt-succ">Présentation</h4>
                                        <h5 class="mx-2 my-0 txt-light">FrameWerk</h5>
                                    </div>
                                    <hr class="my-2">
                                    <div class="nav-down">
                                        
                                        <a href="#" class="butn butn-succ mx-2"><p class="m-0">CSS</p></a>
                                        <a href="#" class="butn butn-succ mx-2"><p class="m-0">JS</p></a>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </header>
                <hr class="my-0">

                <main class="">

                </main>
            </div>
        </div>
        <div class="fluid disp-center"><p>SILENUS.FR &copy; 2022</p><div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/dayNight.js"></script>
    </body>
</html>