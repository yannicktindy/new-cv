<section id="diplome" class="">
    <div class="parallax3 disp-start-bottom">
        <div class="parallax-caption mar1 pad1">
            <h3 class="txt-lum">DIPLOMES & FORMATIONS</h3>
        </div>
    </div>
    <div class="pad3">
        <a class="butn butn-succ" data-bs-toggle="collapse" href="#collapseHB" role="button" aria-expanded="false" aria-controls="collapseExample">
            Développeur Web et Web Mobile (bac+2) <br>
            Human Booster - 2021 / 2022 
        </a>
        <div class="collapse pady1" id="collapseHB">
            <div class="card card-body bk-dark pad2">
                <ul class="txt-lum"> 
                    <li>Symfony, Composer</li>
                    <li>API - WS REST</li>
                    <li>PHP, OO, DO</li>
                    <li>Déploiement Serveur</li>
                    <li>SQL, Bases de données ( Méthode MERISE )</li>
                    <li>Angular, Typescript</li>
                    <li>MVC</li>
                    <li>Webpack</li>
                    <li>Javascript, Ajax</li>
                    <li>HTML5, CSS3, Bootstrap5, Sass</li>
                    <li>Linux, Docker</li>
                    <li>CMS Wordpress</li>
                    <li>Méthode Agile, Scrum</li>
                    <li>Versionning GitLab</li>
                    <li>Maquettage, prototypage figma </li>
                </ul>
            </div>
        </div>
        <hr>

        <a class="butn butn-succ" data-bs-toggle="collapse" href="#collapseUdemy" role="button" aria-expanded="false" aria-controls="collapseExample">
            UDEMY <br>
            Modules - 2020 / 2021 
        </a>
        <div class="collapse pady1" id="collapseUdemy">
            <div class="card card-body bk-dark pad2">
                <ul class="txt-lum"> 
                    <li>Javascript, La formation ULTIME</li>
                    <li>PHP & MySQL, La formation ULTIME</li>
                    <li>SASS & SCSS, La formation ULTIME</li>
                    <li>Bootstap 5, La formation ULTIME</li>
                    <li>Flexbox, La formation ULTIME</li>
                    <li>HTML 5 & CSS 3, La formation ULTIME </li>
                </ul>
            </div>
        </div>
        <hr>

        <a class="butn butn-succ" >
             CFP - Menuiserie <br>
             AFPA 2001
        </a>
        <hr>
        <a class="butn butn-succ" >
            BTS Informatique Industrielle - première année <br>
            1995 / 1996 - C / C++ 
        </a>
        <hr>
        <a class="butn butn-succ" >
            Bac F3 - Electrotechnique <br>
            1994 / 1995 
        </a>
    </div>
</section>