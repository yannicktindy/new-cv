<section id="presentation" class="">
    <div class="parallax1 disp-start-bottom">
        <div class="parallax-caption mar1 pad1">
            <h3 class="txt-lum">PRESENTATION PERSONNELLE</h3>
        </div>
    </div>
    <div class="pad3">

        <h3 class="txt-succ">Du travail du bois au monde du Code</h3>
        <hr>
        <p>J'ai réalisé mon premier site HTML en 2000. J'ai par la suite installé différents CMS Pour des amis et de petites associations dont je faisez parti. Principalement du Wordpress, mais aussi Drupal, Joomla et Spip.</p>
        <hr>
        <p>Un accident de travail** en 2020 m’a permis de transformer une passion en nouveau projet professionnel. J’ai mis à profit ce temps d’arrêt pour accroître mes capacités en développement web. J’ai suivi des modules de développement web avec la plateforme Udemy.</p>
        <hr>
        <p>Pour augmenter mon potentiel, j’ai recherché une formation qualifiante afin d'être parfaitement opérationnel en Fullstack. Le Front-end me stimule, mais c’est le back qui m’attire le plus.
        J'ai obtenu mon titre (Bac+2) de développeur web / web mobile avec Human Booster.</p>
        <hr>
        <p>Depuis la fin de ma formation, je partage mon temps entre la recherche d'emploi et la montée en compétences sur Symfony. J'aime énormément ce framework que je continu d'explorer avec assiduité.</p>
        <hr>
        <p>L'univers de la conception d'application m'appelle. L'avenir est dans le téléphone mobile. Je souhaite donc continuer ma progression et ma montée en compétences, en me formant dans ce domaine.</p>
        <hr>
        <p>Mon expérience professionnelle technique et artisanale font de moi un partenaire précis et rigoureux.<br>
        Mon parcours d’entrepreneur en lutherie m'a enraciné dans l’autonomie. J’ai aquis une grande capacité pour trouver des solutions techniques optimales combinant pertinence et rapidité d’exécution.</p>
        <hr>
        <p>Chaleureux et enthousiaste, j’ai une capacité naturelle à motiver les personnes qui m’entourent.<br>
        Ma capacité d’analyse combinée à ma nature empathique feront de moi un moteur dynamique et fiable au sein de votre équipe.</p>
        <hr>
        <p>** Je suis reconnu comme travailleur handicapé et ceci peut être un avantage pour vous. Vous pouvez bénéficier d’aides. Je ne peux simplement plus porter de charges, mais un poste de développeur ne nécessite aucun aménagement particulier.
        </p>

        <div class="pad2 text-center"> 
            <hr class="hr-dl">
            <h3 class="txt-dl">Hobbies :</h3>
            <div class="butn butn-succ dayNight m-2">Ecriture</div><br>
            <div class="butn butn-succ dayNight m-2">Symfony</div><br>
            <div class="butn butn-succ dayNight m-2">Lutherie</div><br>
            <div class="butn butn-succ dayNight m-2">Permaculture</div>
            <hr class="hr-dl">
        <div>     
       
    </div>
</section>