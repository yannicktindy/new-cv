<section id="symfony" class="">
    <div class="parallax2 disp-start-bottom">
        <div class="parallax-caption mar1 pad1">
            <h3 class="txt-lum">PASSION SYMFONY</h3>
        </div>
    </div>
    <div class="pad3" >
        <div class="symfony mary2">
            <img src="img/square/symfony.png" alt="symfony" class="img-fluid">
        </div>
        <p>Depuis la fin de ma formation, j'ai partagé mon temps entre la recherche d'emploi et la <span class="txt-succ">montée en compétences</span> sur Symfony. <br>
            J'aime énormément ce framework que je continue d'explorer avec assiduité. <br>
            De ce fait, même si je m'oriente vers la CDA en JAVA / ANGULAR, je souhaite garder la posibilité de continuer à travailler sur Symfony. <br>
        </p>

        <p>
            <button class="butn butn-succ" type="button" data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" aria-expanded="false" aria-controls="collapseWidthExample">
                <h5>Framework & Bundles</h5>
            </button>
        </p>
        <div>
            <div class="collapse collapse-horizontal" id="collapseWidthExample">
                <div class="card card-body bk-dark pad2">
                    <p class="txt-lum">
                        - <strong class="txt-succ">Doctrine</strong> ORM, Entity, Database création, migrations, QueryBuilder, CRUDs<br>
                        - <strong class="txt-succ">Routing Controlleurs</strong>, securité des routes <br>
                        - <strong class="txt-succ">Servicing</strong> Creations de services, inversion de contrôle et injection <br>
                        - <strong class="txt-succ">Twig</strong> Templating, filtres <br>
                        - <strong class="txt-succ">Encore</strong> Webpack <br>
                        - <strong class="txt-succ">User</strong> Make, auth, login & register (karser-recaptcha3-bundle) <br>
                        - <strong class="txt-succ">Formulaires</strong> createForm, creatView & handleRequest <br>
                        - <strong class="txt-succ">Mailer</strong> Envoi d'email <br>
                        - <strong class="txt-succ">Easyadmin</strong> Dashboard & Cruds <br>
                        - <strong class="txt-succ">API Plateform</strong> + LexikJWTAuthenticationBundle <br>
                        - <strong class="txt-succ">Symfony UX</strong> Stimulus & Turbo <br>
                        - <strong class="txt-succ">File Upload</strong> Importation de fichiers images ou autres <br>
                        - <strong class="txt-succ">Fixtures</strong> Alice Bundle <br>
                        - <strong class="txt-succ">KNP Snappy</strong> génération de pdf <br>
                        - <strong class="txt-succ">DomPDF</strong> génération de pdf <br>
                        - <strong class="txt-succ">LippImagine</strong> gestionaire d'optimisation des images <br>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>