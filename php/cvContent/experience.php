<section id="experiences" class="">
    <div class="parallax4 disp-start-bottom">
        <div class="parallax-caption mar1 pad1">
            <h3 class="txt-lum">EXPERIENCES PROFESSIONNELLES</h3>
        </div>
    </div>
    <div class="disp-col-between pad3">
        <div class="job bk-dark">
            <img src="img/16-9/band0.png" alt="logo APC" class="fluid">
            <div class="text-center pad1">
                <h5 class="txt-succ">Développement Web</h5>
                <h6 class="txt-lum">Symfony 6</h6>
                <button type="button" class="butn butn-succ pad2" data-bs-toggle="modal" data-bs-target="#devModal">
                    Voir
                </button>
            </div>
        </div>

        <!-- Modal dev -->
        <div class="modal fade" id="devModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content bk-dl pad2">
                    <h5 class="txt-succ">Développement Web</h5>
                    <h4 class="txt-dl">Symfony 6.1 - PHP 8.1</h4>
                    <p class="txt-dl">
                        Gaëtan Thomas : Développeur Indépendant <br>
                        Stage 2022 (octobre/novembre)
                    </p>
                    <hr class="hr-dl">
                    <p class="txt-dl mar1">
                        <span class="txt-succ"><strong>- Refonte :</strong></span> d'un site vitrine d'une école de musique : apausecroche.fr <br><br>
                        <span class="txt-succ"><strong>- Upgrade :</strong></span> Mise à niveau du projet initial en Symfony 5.3 - PHP 7.4 en Symfony 6.1 - PHP 8.1. Utilisation du bundle Rector et correction des syntaxes <br><br>
                        <span class="txt-succ"><strong>- Optimisation :</strong></span> Nettoyage du code, implémentation des balises html sémeantique spécifiques, implementtation de metaOG en variables. Installation du Bundle Liip Imagine pour optimiser les images <br><br>
                        <span class="txt-succ"><strong>- Easyadmin :</strong></span> Installation du bundle Easyadmin et configuration complète du back-end (CRUDs) et de l'interface administratif. Génération de fichiers PDF avec le Bundle Snappy <br><br>
                        <span class="txt-succ"><strong>- Calendrier :</strong></span> Refonte du calendrier d'évènements (fullstack), création en base de données, crud controller, front controller et dynamique Javascript avec stimulus (cette feature n'est pas encore déployée) <br><br>
                        <span class="txt-succ"><strong>- Web dynamique :</strong></span> Javascript passé en différents controlleurs Stimulus <br><br>
                        <span class="txt-succ"><strong>- Features :</strong></span> Réalisation de différentes features telles qu'un accordéon en stimulus pour le FAQ... <br><br>
                    </p>
                    <hr class="hr-dl">
                    <button type="button" class="butn butn-dl pad2" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

        <hr class="hr-dl">

        <!-- Button trigger modal art -->
        <div class="job bk-dark">
            <img src="img/16-9/craft.png" alt="logo APC" class="fluid">
            <div class="text-center pad1">
                <h5 class="txt-succ">Artisanat & Mise en Oeuvre</h5>
                <h6 class="txt-lum">Menuisier / tourneur sur bois / luthier</h6>
                <button type="button" class="butn butn-succ pad2" data-bs-toggle="modal" data-bs-target="#artModal">
                    Voir
                </button>
            </div>
        </div>

        <!-- Modal art-->
        <div class="modal fade" id="artModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content bk-dl pad2">
                    <h5 class="txt-succ">Artisanat & Mise en Oeuvre</h5>
                    <h4 class="txt-dl">Menuisier / tourneur sur bois / luthier</h4>
                    <hr class="hr-dl">
                    <p class="txt-dl mar1">
                        <span class="txt-succ"><strong>- Gestion de projet</strong></span><br><br>
                        <span class="txt-succ"><strong>- Plans :</strong></span> lecture, analyse<br><br>
                        <span class="txt-succ"><strong>- Analyse de produits :</strong></span> création de plans, réalisation de prototypes<br><br>
                        <span class="txt-succ"><strong>- Éco-responsabilité :</strong></span> Connaître, favoriser et valoriser les essences de bois locales, mise au points de produits de traitements bios<br><br>
                        <span class="txt-succ"><strong>- Processus :</strong></span> optimisation des temps exécution, réalisations de gabarits<br><br>
                        <span class="txt-succ"><strong>- Outils :</strong></span> recherches historiques, expérimentations, créations d’outils ultra-spécifiques<br><br>
                        <span class="txt-succ"><strong>- Normalisation :</strong></span> stabilisations des processus et des produits<br><br>
                        <span class="txt-succ"><strong>- Réglage :</strong></span> <br><br>
                    </p>
                    <hr class="hr-dl">
                    <p class="txt-dl mar1">
                        <span class="txt-succ">2019 / 2020 :</span><strong> Menuisierie / ebenisterie</strong><br>
                        Manpower<br><br>
                        <span class="txt-succ">2007 / 2017 :</span><strong> Entrepreneur en lutherie</strong><br>
                        Cornemuses et épinettes - Fournisseur pour le conservatoires d'Amiens et de Beauvais<br><br>
                        <span class="txt-succ">2004 / 2006 :</span><strong> Sculpteur, tourneur sur bois</strong><br>
                        Artiste Indépendant<br><br>
                        <span class="txt-succ">2004 / 2002 :</span><strong> Assistant de production en design</strong><br>
                        Editions Limitées Paris<br><br>
                    </p>
                    <hr class="hr-dl">

                    <button type="button" class="butn butn-dl pad2" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

        <hr class="hr-dl">
        <!-- Button trigger modal conf -->
        <div class="job bk-dark">
            <img src="img/16-9/perma.png" alt="logo APC" class="fluid">
            <div class="text-center pad1">
                <h5 class="txt-succ">Pédagogie & Techniques</h5>
                <h6 class="txt-lum">Conférencier / formateur en permaculture</h6>
                <button type="button" class="butn butn-succ pad2" data-bs-toggle="modal" data-bs-target="#confModal">
                    Voir
                </button>
            </div>
        </div>

        <!-- Modal conf-->
        <div class="modal fade" id="confModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content bk-dl pad2">
                    <h5 class="txt-succ">Pédagogie & Techniques</h5>
                    <h4 class="txt-dl">Conférencier / formateur en permaculture</h4>
                    <hr class="hr-dl">
                    <p class="txt-dl mar1">
                        <span class="txt-succ"><strong>- Gestion de projet</strong></span><br><br>
                        <span class="txt-succ"><strong>- Analyse de la spécificité des terrains</strong></span> <br><br>
                        <span class="txt-succ"><strong>- Expérimentations de techniques ancestrales</strong></span> <br><br>
                        <span class="txt-succ"><strong>- Éco-responsabilité : </strong></span> Organisation du collectage et valorisations des dechets organiques locaux<br><br>                        
                        <span class="txt-succ"><strong>- Prospectives sur les dernières innovations et recherches</strong></span><br><br>
                        <span class="txt-succ"><strong>- Transformer des pratiques expérimentales en techniques concrètes, pragmatiques et applicables</strong></span> <br><br>
                        <span class="txt-succ"><strong>- Réalisation de contenu pédagogique</strong></span> <br><br>
                        <span class="txt-succ"><strong>- Enseignement et conférences</strong></span> <br><br>
                        <span class="txt-succ"><strong>- Mise en place de jardins potagers participatifs pour des communes et des communautés de communes</strong></span> <br><br>
                    </p>
                    <hr class="hr-dl">
                    <p class="txt-dl mar1">
                        <span class="txt-succ">2017 / 2018 :</span><strong> Projet de mise en place d'un centre de formation en permaculture</strong><br>
                        Pothus Robinson<br><br>
                        <span class="txt-succ">2014 / 2018 :</span><strong> Formateur, conférencier en permaculture</strong><br>
                        CPIE Vallée de Somme et Lycée agricole CFPPA du Paraclet et de Péronne<br><br>
                    </p>
                    <hr class="hr-dl">
                    <button type="button" class="butn butn-dl pad2" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        <div class="pad2 text-center"> 
            <hr class="hr-dl">
            <h3 class="txt-dl">Soft Skills :</h3>
            <div class="butn butn-succ dayNight m-2">Analytique / Synthétique</div><br>
            <div class="butn butn-succ dayNight m-2">Empathique</div><br>
            <div class="butn butn-succ dayNight m-2">Enthousiaste</div><br>
            <hr class="hr-dl">
        </div>
    </div>
</section>