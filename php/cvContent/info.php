<section id="information" class="">
    <div class="parallax2 disp-start-bottom">
        <div class="parallax-caption mar1 pad1">
            <h3 class="txt-lum">INFORMATIONS & LIENS</h3>
        </div>
    </div>
    <div class="pad3">
        <h4 class="txt-dl">Liens</h4>
        <a href="https://gitlab.com/yannicktindy" target="_blank" class="butn butn-succ dayNight m-2">GitLab</a>
        <a href="https://www.linkedin.com/in/yannick-tindy/"  target="_blank" class="butn butn-succ dayNight m-2">LinkedIn</a>
        <a href="doc/CV-yannick-tindy.pdf"  target="_blank" class="butn butn-succ dayNight m-2">CV Papier</a>
        <hr>
        
        <h3 class="txt-succ">Informations</h3>
        <div class="butn butn-out-succ butn-glow mary1"> Maj : 10/05/2023</div>
        <p class="txt-dl m-0">- Ce site est la nouvelle version de mon CV en ligne</p> 
        <p class="txt-dl m-0">- Premier déploiement : 09 mai 2023</p> 
        <p class="txt-dl m-0">- Il est developpé sur VsCode en : HTML 5, CSS 3, Bootstrap, PHP, JavaScript Vanilla</p> 
        <p class="txt-dl m-0">- Les images sont générées avec Midjourney</p> 
        <p class="txt-dl m-0">- Il n'est pas completement terminé</p> 
        <p class="txt-dl m-0">- J'ai passé beaucoup de temps sur PHP Symfony, c'est le moment pour moi de remettre un coup de cravache sur le front-end</p> 
        <p class="txt-dl m-0">- Ce CV sera rapidement amélioré et je vais presenter un collector de composants CSS et JavaScript</p>
        <hr>
    </div>
</section>