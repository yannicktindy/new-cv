<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cv Yannick Tindy</title>
        <meta name="author" content="Yannick Tindy">
        <meta property="og:type" content="Yannick Tindy">
        <meta property="og:title" content="Yannick Tindy - Développeur Web / Web mobile Fullstack (Bac+2)">
        <meta property="og:url" content="https://silenus.fr">
        <meta property="og:image" content="https://silenus.fr/img/square/profile.jpg">
        <meta property="og:description" content="Objectif : Concepteur / Développeur d'Applications. Recherche d'un contrat en alternance (Bac+3/4)">
        <meta name="theme-color" content="#00FF00">
        <link rel="icon" type="image/x-icon" href="img/square/favicon.png" />

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">

        <link rel="stylesheet" href="style/mainBase/color.css">
        <link rel="stylesheet" href="style/mainBase/font.css">        
        <link rel="stylesheet" href="style/mainBase/margin.css">
        <link rel="stylesheet" href="style/mainBase/display.css">

        <link rel="stylesheet" href="style/components/buttons.css">

        <link rel="stylesheet" href="style/dark.css">
        <link rel="stylesheet" href="style/light.css">
        <link rel="stylesheet" href="style/aside.css">
        <link rel="stylesheet" href="style/main.css">
        <link rel="stylesheet" href="style/general.css">
    </head>

    <body class="dark">
        <div class="cv-constainer">
            <aside class="bk-dl fixed-top marx3">
                <?php include 'php/asideDesk.php'; ?>
            </aside>
            <div class="under-aside"></div>
            <!-- <div class="btn btn-success dayNight">Mode ☀️</div></aside> -->
            <main class="bk-dl">
            <?php include 'php/mainDesk.php'; ?>
            </main>
        </div>
        <div class="fluid disp-center">SILENUS.FR &copy; 2022<div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/dayNight.js"></script>
    </body>
</html>

                        <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script> -->